# Guestbook-web

Ejemplo de portlet Liferay con acceso a servicios locales e implementación de un portlet de administración:

+ Implementación del [tutorial](https://portal.liferay.dev/docs/7-0/tutorials/-/knowledge_base/t/writing-your-first-liferay-application).
+ Pruebas de llamadas Ajax a *MVCResourceCommand* usando *AUI*.
+ Prueba de manejo de formularios.
+ Aplicación de *FriendlyUrls*.