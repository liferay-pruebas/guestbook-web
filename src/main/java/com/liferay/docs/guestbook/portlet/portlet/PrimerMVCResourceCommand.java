package com.liferay.docs.guestbook.portlet.portlet;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.docs.guestbook.portlet.constants.GuestbookPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;

@Component(
	    property = {
	        "javax.portlet.name=" + GuestbookPortletKeys.GUESTBOOK,
	        "mvc.command.name=primero"
	    },
	    service = MVCResourceCommand.class
	)
public class PrimerMVCResourceCommand implements MVCResourceCommand{
	
	@Override
	public boolean serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException {
		try {
		System.out.println("llamada ajax a PrimerMVCResourceCommand ");
			response.setContentType("text");

			response.resetBuffer();
			response.getWriter().print("Hola desde el primer portlet con ajax");		
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
			response.setProperty(ResourceResponse.HTTP_STATUS_CODE, "500");
			System.err.println("error al procesar peticion de recurso");
			return true;
		}
		return false;
	}
}
