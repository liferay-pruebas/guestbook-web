package com.liferay.docs.guestbook.portlet.url;

import org.osgi.service.component.annotations.Component;

import com.liferay.docs.guestbook.portlet.constants.GuestbookPortletKeys;
import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;
import com.liferay.portal.kernel.portlet.FriendlyURLMapper;


@Component(
	     property = {
	         "com.liferay.portlet.friendly-url-routes=META-INF/friendly-url-routes/routes.xml",
	         "javax.portlet.name=" + GuestbookPortletKeys.GUESTBOOK
	     },
	     service = FriendlyURLMapper.class
	 )
public class GuestbooksFriendlyURLMapper extends DefaultFriendlyURLMapper {

    @Override
    public String getMapping() {
        return _MAPPING;
    }

    private static final String _MAPPING = "guestbooks";

}
