<%@include file="../init.jsp"%>

<%
	long guestbookId = Long.valueOf((Long) renderRequest.getAttribute("guestbookId"));
	String mvcPath = ParamUtil.getString(request, "mvcPath");

	ResultRow row = (ResultRow) request.getAttribute("SEARCH_CONTAINER_RESULT_ROW");

	Entry entry = (Entry) row.getObject();
%>

<liferay-ui:icon-menu>

		<portlet:renderURL var="editEntryURL">
			<portlet:param name="mvcPath"
				value="/guestbookwebportlet/edit_entry.jsp" />
			<portlet:param name="guestbookId"
				value="<%=String.valueOf(guestbookId)%>" />
			<portlet:param name="entryId"
				value="<%=String.valueOf(entry.getEntryId())%>" />
		</portlet:renderURL>

	<liferay-ui:icon image="edit" message="Edit entry"
		url="<%=editEntryURL.toString()%>" />

	<portlet:actionURL name="deleteEntry" var="deleteURL">
		<portlet:param name="guestbookId"
			value="<%=String.valueOf(guestbookId)%>" />
		<portlet:param name="entryId"
				value="<%=String.valueOf(entry.getEntryId())%>" />
	</portlet:actionURL>

	<liferay-ui:icon image="delete" message="Delete entry" url="<%=deleteURL.toString()%>" />

</liferay-ui:icon-menu>