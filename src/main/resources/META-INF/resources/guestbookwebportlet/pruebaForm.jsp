<%@include file="../init.jsp"%>

<liferay-ui:error key="nomNoA" message="El nombre no empieza por A"/>

<portlet:actionURL name="pruebaForm" var="formUrl"></portlet:actionURL>

<h4>Prueba de formulario AUI</h4>

<aui:form action="<%=formUrl%>" name="<portlet:namespace />fm">
	<aui:input label="Nombre" type="text" name="nombre">
        <aui:validator name="required"/>
	</aui:input>
	
	<aui:input label="Apellido" name="apellido" type="text">
        <aui:validator name="required"/>
        </aui:input>	
	
	<aui:button type="submit"></aui:button>

</aui:form>

<hr/>

<h4>Prueba de formulario plano </h4>

<form action="<%=formUrl%>" method="post" name="<portlet:namespace />fm">
	<div class="form-group">
	<label for="<portlet:namespace/>nombre">Nombre:</label>
	<input class="form-control" type="text" name="<portlet:namespace/>nombre"/>
	</div>
	<div class="form-group">
	<label for="<portlet:namespace/>apellido">Apellido:</label>
	<input class="form-control" type="text" name="<portlet:namespace/>apellido"/>
	</div>
	<button class="btn btn-default" type="submit">Enviar</button>
</form>