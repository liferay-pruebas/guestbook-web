<%@include file="../init.jsp"%>

<liferay-ui:success key="entryAdded" message="entry-added" />
<liferay-ui:success key="entryUdated" message="entry-updated" />
<liferay-ui:success key="guestbookAdded" message="guestbook-added" />
<liferay-ui:success key="entryDeleted" message="entry-deleted" />
<liferay-ui:error key="" message="error"/>


<liferay-ui:success key="datosProcesados" message="Datos procesados..." />

<%
	long guestbookId = Long.valueOf((Long) renderRequest.getAttribute("guestbookId"));
%>

<portlet:renderURL var="addEntryURL">
	<portlet:param name="mvcPath" value="/guestbookwebportlet/edit_entry.jsp" />
	<portlet:param name="guestbookId" value="<%=String.valueOf(guestbookId)%>" />
</portlet:renderURL>

<aui:button-row cssClass="guestbook-buttons">
	<%--<aui:button onClick="<%=addEntryURL.toString()%>" value="Add Entry"></aui:button> no me va en el LF CE remoto--%>
	<a class="btn btn-default" href="<%=addEntryURL%>">Add Entry</a>
</aui:button-row>


<liferay-ui:search-container total="<%=EntryLocalServiceUtil.getEntriesCount()%>">
<liferay-ui:search-container-results results="<%=EntryLocalServiceUtil.getEntries(scopeGroupId.longValue(), guestbookId, searchContainer.getStart(), searchContainer.getEnd())%>" />

<liferay-ui:search-container-row
    className="com.liferay.docs.guestbook.model.Entry" modelVar="entry">

    <liferay-ui:search-container-column-text property="message" />

    <liferay-ui:search-container-column-text property="name" />
    
    <liferay-ui:search-container-column-jsp
            align="right" 
            path="/guestbookwebportlet/entry_actions.jsp" />
</liferay-ui:search-container-row>

<liferay-ui:search-iterator />

</liferay-ui:search-container>

<hr/>

<h3>Pruebas de formularios</h3>

<portlet:renderURL var="formPrueba">
	<portlet:param name="mvcPath" value="/guestbookwebportlet/pruebaForm.jsp" />
</portlet:renderURL>

<%--<aui:button onClick="<%=formPrueba.toString()%>" value="Probar formulario"></aui:button>  no me va en el LF CE remoto--%>
<a class="btn btn-default" href="<%=formPrueba%>">probar formulario</a>

<hr/>

<h3>Prueba de ajax</h3>

<portlet:resourceURL id="primero" var="ajaxResourceUrl"/>
<portlet:resourceURL id="saludar" var="otherAjaxResourceUrl"/>


<script type="text/javascript">

function callServeResource(url){
    AUI().use('aui-io-request', function(A){
        A.io.request(url, {
               method: 'post',
               data: {
               },
               on: {
                   success: function() {
                    alert(this.get('responseData'));
                   }
              }
        });
     });
}
</script>

<button class="btn btn-primary" onclick="callServeResource('<%=ajaxResourceUrl%>')">Primer mvcResource ajax</button>	
<button class="btn btn-primary" onclick="callServeResource('<%=otherAjaxResourceUrl%>')">Segundo mvcResource ajax</button>
